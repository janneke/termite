package termite

import (
	"errors"
	"fmt"
	"github.com/hanwen/termite/stats"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"os"
	"sort"
	"syscall"
	"sync"
	"time"
)

var _ = log.Println

type Registration struct {
	Address string
	Name    string
	Version string
}

type Registered struct {
	Registrations []Registration
}

type WorkerRegistration struct {
	Registration
	LastReported time.Time
}

// Coordinator is the registration service for termite.  Workers
// register here.  A master looking for workers contacts the
// Coordinator to fetch a list of available workers.  In addition, it
// has a HTTP interface to inspect each worker.
type Coordinator struct {
	Mux *http.ServeMux

	options *CoordinatorOptions

	listener net.Listener
	mutex    sync.Mutex
	workers  map[string]*WorkerRegistration
}

type CoordinatorOptions struct {
	// Secret is the password for coordinator, workers and master
	// to authenticate.
	Secret []byte

	// Password should be passed in the kill/restart URLs to make
	// sure web scrapers don't randomly shutdown workers.
	WebPassword string
}

func NewCoordinator(opts *CoordinatorOptions) *Coordinator {
	o := *opts
	return &Coordinator{
		options: &o,
		workers: make(map[string]*WorkerRegistration),
		Mux:     http.NewServeMux(),
	}
}

func (me *Coordinator) Register(req *Registration, rep *int) error {
	conn, err := DialTypedConnection(req.Address, RPC_CHANNEL, me.options.Secret)
	if conn != nil {
		conn.Close()
	}
	if err != nil {
		return errors.New(fmt.Sprintf(
			"error contacting address: %v", err))
	}

	me.mutex.Lock()
	defer me.mutex.Unlock()

	w := &WorkerRegistration{Registration: *req}
	w.LastReported = time.Now()
	me.workers[w.Address] = w
	return nil
}

func (me *Coordinator) WorkerCount() int {
	me.mutex.Lock()
	defer me.mutex.Unlock()
	return len(me.workers)
}

func (me *Coordinator) List(req *int, rep *Registered) error {
	me.mutex.Lock()
	defer me.mutex.Unlock()

	keys := []string{}
	for k := range me.workers {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _, k := range keys {
		w := me.workers[k]
		rep.Registrations = append(rep.Registrations, w.Registration)
	}
	return nil
}

func (me *Coordinator) checkReachable() {
	now := time.Now()

	addrs := me.workerAddresses()

	var toDelete []string
	for _, a := range addrs {
		conn, err := DialTypedConnection(a, RPC_CHANNEL, me.options.Secret)
		if err != nil {
			toDelete = append(toDelete, a)
		} else {
			conn.Close()
		}
	}

	me.mutex.Lock()
	for _, a := range toDelete {
		w := me.workers[a]
		if w != nil && now.After(w.LastReported) {
			delete(me.workers, a)
		}
	}
	me.mutex.Unlock()
}

const _POLL = 60

func (me *Coordinator) PeriodicCheck() {
	for {
		c := time.After(_POLL * 1e9)
		<-c
		me.checkReachable()
	}
}

func (me *Coordinator) rootHandler(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	me.mutex.Lock()
	defer me.mutex.Unlock()

	fmt.Fprintf(w, "<html><head><title>Termite coordinator</title></head>")
	fmt.Fprintf(w, "<body><h1>Termite coordinator</h1><ul>")
	fmt.Fprintf(w, "<p>version %s", Version())
	defer fmt.Fprintf(w, "</body></html>")

	keys := []string{}
	for k := range me.workers {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, k := range keys {
		worker := me.workers[k]
		fmt.Fprintf(w, "<li><a href=\"worker?host=%s\">address <tt>%s</tt>, host <tt>%s</tt></a>",
			worker.Address, worker.Address, worker.Name)
	}
	fmt.Fprintf(w, "</ul>")

	fmt.Fprintf(w, "<hr><p><a href=\"killall\">kill all workers,</a>"+
		"<a href=\"restartall\">restart all workers</a>")
}

func (me *Coordinator) shutdownSelf(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "<html><head><title>Termite coordinator</title></head>")
	fmt.Fprintf(w, "<body><h1>Shutdown in progress</h1><ul>")

	// Async, so we can still return the reply here.
	time.AfterFunc(100e6, func() { me.Shutdown() })
}

func (me *Coordinator) killAll(restart bool) error {
	addrs := me.workerAddresses()
	done := make(chan error, len(addrs))
	for _, w := range addrs {
		go func(w string) {
			err := me.killWorker(w, restart)
			done <- err
		}(w)
	}

	errs := []error{}
	for _ = range addrs {
		e := <-done
		if e != nil {
			errs = append(errs, e)
		}
	}
	if len(errs) > 0 {
		return fmt.Errorf("%v", errs)
	}
	return nil
}

func (me *Coordinator) killWorker(addr string, restart bool) error {
	conn, err := DialTypedConnection(addr, RPC_CHANNEL, me.options.Secret)
	if err == nil {
		killReq := ShutdownRequest{Restart: restart}
		rep := ShutdownResponse{}
		cl := rpc.NewClient(conn)
		defer cl.Close()
		err = cl.Call("Worker.Shutdown", &killReq, &rep)
	}
	return err
}

func (me *Coordinator) killAllHandler(w http.ResponseWriter, req *http.Request) {
	me.log(req)
	if !me.checkPassword(w, req) {
		return
	}

	w.Header().Set("Content-Type", "text/html")
	fmt.Fprintf(w, "<p>%s in progress", req.URL.Path)
	err := me.killAll(req.URL.Path == "/restartall")
	if err != nil {
		fmt.Fprintf(w, "error: %v", err)
	}
	// Should have a redirect.
	fmt.Fprintf(w, "<p><a href=\"/\">back to index</a>")
	go me.checkReachable()
}

func (me *Coordinator) checkPassword(w http.ResponseWriter, req *http.Request) bool {
	if me.options.WebPassword == "" {
		return true
	}
	q := req.URL.Query()
	pw := q["pw"]
	if len(pw) == 0 || pw[0] != me.options.WebPassword {
		fmt.Fprintf(w, "<html><body>unauthorized &amp;pw=PASSWORD missing or incorrect.</body></html>")
		return false
	}
	return true
}

func (me *Coordinator) killHandler(w http.ResponseWriter, req *http.Request) {
	me.log(req)
	if !me.checkPassword(w, req) {
		return
	}

	addr, conn, err := me.getHost(req)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "<html><head><title>Termite worker error</title></head>")
		fmt.Fprintf(w, "<body>Error: %s</body></html>", err.Error())
		return
	}
	defer conn.Close()

	w.Header().Set("Content-Type", "text/html")
	restart := req.URL.Path == "/restart"
	fmt.Fprintf(w, "<html><head><title>Termite worker status</title></head>")
	fmt.Fprintf(w, "<body><h1>Status %s</h1>", addr)
	defer fmt.Fprintf(w, "</body></html>")

	killReq := ShutdownRequest{
		Restart: restart,
		Kill:    !restart,
	}
	rep := ShutdownResponse{}
	cl := rpc.NewClient(conn)
	defer cl.Close()
	err = cl.Call("Worker.Shutdown", &killReq, &rep)
	if err != nil {
		fmt.Fprintf(w, "<p><tt>Error: %v<tt>", err)
		return
	}

	action := "kill"
	if restart {
		action = "restart"
	}
	fmt.Fprintf(w, "<p>%s of %s in progress", action, conn.RemoteAddr())
	// Should have a redirect.
	fmt.Fprintf(w, "<p><a href=\"/\">back to index</a>")
	go me.checkReachable()
}

func (me *Coordinator) shutdownWorker(addr string, restart bool) error {
	conn, err := DialTypedConnection(addr, RPC_CHANNEL, me.options.Secret)
	if err != nil {
		return err
	}

	killReq := ShutdownRequest{Restart: restart}
	rep := ShutdownResponse{}
	cl := rpc.NewClient(conn)
	err = cl.Call("Worker.Shutdown", &killReq, &rep)
	cl.Close()
	conn.Close()
	return err
}

func (me *Coordinator) workerAddresses() (out []string) {
	me.mutex.Lock()
	defer me.mutex.Unlock()
	for k := range me.workers {
		out = append(out, k)
	}
	return out
}

func (me *Coordinator) haveWorker(addr string) bool {
	me.mutex.Lock()
	defer me.mutex.Unlock()
	_, ok := me.workers[addr]
	return ok
}

func (me *Coordinator) getHost(req *http.Request) (string, net.Conn, error) {
	q := req.URL.Query()
	vs, ok := q["host"]
	if !ok || len(vs) == 0 {
		return "", nil, fmt.Errorf("query param 'host' missing")
	}
	addr := string(vs[0])
	if !me.haveWorker(addr) {
		return "", nil, fmt.Errorf("worker %q unknown", addr)
	}

	conn, err := DialTypedConnection(addr, RPC_CHANNEL, me.options.Secret)
	if err != nil {
		return "", nil, fmt.Errorf("error dialing: %v", err)
	}

	return addr, conn, nil
}

func (me *Coordinator) logHandler(w http.ResponseWriter, req *http.Request) {
	_, conn, err := me.getHost(req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "<html><head><title>Termite worker error</title></head>")
		fmt.Fprintf(w, "<body>Error: %s</body></html>", err.Error())
		return
	}
	sz := int64(500 * 1024)
	sizeStr, ok := req.URL.Query()["size"]
	if ok {
		fmt.Scanf(sizeStr[0], "%d", &sz)
	}

	logReq := LogRequest{Whence: os.SEEK_END, Off: -sz, Size: sz}
	logRep := LogResponse{}
	client := rpc.NewClient(conn)
	err = client.Call("Worker.Log", &logReq, &logRep)
	client.Close()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "<html><head><title>Termite worker error</title></head>")
		fmt.Fprintf(w, "<body>Error: %s</body></html>", err.Error())
		return
	}

	w.Header().Set("Content-Type", "text/plain")
	w.Write(logRep.Data)
	return
}

func (me *Coordinator) workerHandler(w http.ResponseWriter, req *http.Request) {
	addr, conn, err := me.getHost(req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "<html><head><title>Termite worker status</title></head>")
		fmt.Fprintf(w, "<body>Error: %s</body></html>", err.Error())
		return
	}

	statusReq := WorkerStatusRequest{}
	status := WorkerStatusResponse{}

	client := rpc.NewClient(conn)
	err = client.Call("Worker.Status", &statusReq, &status)
	client.Close()
	if err != nil {
		fmt.Fprintf(w, "<p><tt>RPC error: %v<tt>\n", err)
		return
	}

	fmt.Fprintf(w, "<p>Worker %s<p>Version %s<p>Jobs %d\n",
		addr, status.Version, status.MaxJobCount)
	fmt.Fprintf(w, "<p><a href=\"/log?host=%s\">Worker log %s</a>\n", addr, addr)

	if !status.Accepting {
		fmt.Fprintf(w, "<b>shutting down</b>")
	}
	stats.CpuStatsWriteHttp(w, status.CpuStats, status.DiskStats)

	fmt.Fprintf(w, "<p>Total CPU: %s", status.TotalCpu.Percent())
	fmt.Fprintf(w, "<p>Content cache hit rate: %.0f %%, Age %d",
		100.0*status.ContentCacheHitRate,
		status.ContentCacheHitAge)

	m := status.MemStat
	fmt.Fprintf(w, "<p>HeapIdle: %v, HeapInUse: %v",
		m.HeapIdle, m.HeapInuse)

	stats.CountStatsWriteHttp(w, status.PhaseNames, status.PhaseCounts)

	for _, mirrorStatus := range status.MirrorStatus {
		me.mirrorStatusHtml(w, mirrorStatus)
	}
	fmt.Fprintf(w, "<p><a href=\"/workerkill?host=%s\">Kill worker %s</a>\n", addr, addr)
	fmt.Fprintf(w, "<p><a href=\"/restart?host=%s\">Restart worker %s</a>\n", addr, addr)
	conn.Close()
}

func (me *Coordinator) mirrorStatusHtml(w http.ResponseWriter, s MirrorStatusResponse) {
	fmt.Fprintf(w, "<h2>Mirror %s</h2>", s.Root)
	for _, s := range s.RpcTimings {
		fmt.Fprintf(w, "<li>%s", s)
	}

	running := 0
	for _, fs := range s.Fses {
		running += len(fs.Tasks)
	}

	fmt.Fprintf(w, "<p>%d maximum jobs, %d running, %d waiting tasks, %d unused filesystems.\n",
		s.Granted, running, s.WaitingTasks, s.IdleFses)
	if !s.Accepting {
		fmt.Fprintf(w, "<p><b>shutting down</b>\n")
	}

	fmt.Fprintf(w, "<ul>\n")
	for _, v := range s.Fses {
		fmt.Fprintf(w, "<li>id %s: %s<ul>\n", v.Id, v.Mem)
		for _, t := range v.Tasks {
			fmt.Fprintf(w, "<li>%s\n", t)
		}
		fmt.Fprintf(w, "</ul>\n")
	}
	fmt.Fprintf(w, "</ul>\n")
}

func (me *Coordinator) Shutdown() {
	log.Println("Coordinator shutdown.")
	me.listener.Close()
}

func (me *Coordinator) log(req *http.Request) {
	log.Printf("from %v: %v", req.RemoteAddr, req.URL)
}

func (me *Coordinator) ServeHTTP(port int) {
	me.Mux.HandleFunc("/",
		func(w http.ResponseWriter, req *http.Request) {
			me.rootHandler(w, req)
		})
	me.Mux.HandleFunc("/worker",
		func(w http.ResponseWriter, req *http.Request) {
			me.workerHandler(w, req)
		})
	me.Mux.HandleFunc("/log",
		func(w http.ResponseWriter, req *http.Request) {
			me.logHandler(w, req)
		})
	me.Mux.HandleFunc("/shutdown",
		func(w http.ResponseWriter, req *http.Request) {
			me.shutdownSelf(w, req)
		})
	me.Mux.HandleFunc("/workerkill",
		func(w http.ResponseWriter, req *http.Request) {
			me.killHandler(w, req)
		})
	me.Mux.HandleFunc("/killall",
		func(w http.ResponseWriter, req *http.Request) {
			me.killAllHandler(w, req)
		})
	me.Mux.HandleFunc("/restartall",
		func(w http.ResponseWriter, req *http.Request) {
			me.killAllHandler(w, req)
		})
	me.Mux.HandleFunc("/restart",
		func(w http.ResponseWriter, req *http.Request) {
			me.killHandler(w, req)
		})

	rpcServer := rpc.NewServer()
	if err := rpcServer.Register(me); err != nil {
		log.Fatal("rpcServer.Register:", err)
	}
	me.Mux.HandleFunc(rpc.DefaultRPCPath,
		func(w http.ResponseWriter, req *http.Request) {
			rpcServer.ServeHTTP(w, req)
		})

	addr := fmt.Sprintf(":%d", port)
	var err error
	me.listener, err = net.Listen("tcp", addr)
	if err != nil {
		log.Fatal("net.Listen: ", err.Error())
	}
	log.Println("Coordinator listening on", addr)

	httpServer := http.Server{
		Addr:    addr,
		Handler: me.Mux,
	}
	err = httpServer.Serve(me.listener)
	if e, ok := err.(*net.OpError); ok && e.Err == syscall.EINVAL {
		return
	}

	if err != nil && err != syscall.EINVAL {
		log.Println("httpServer.Serve:", err)
	}
}
